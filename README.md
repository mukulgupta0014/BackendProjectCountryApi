# BackendProjectCountryApi


This project is a Spring Boot Backend  that provides various endpoints for managing and retrieving information about countries, including their population, area, and the languages spoken. It also includes authentication and authorization features using JWT tokens.

## Prerequisites

- Java 17 
- Maven 3.6 or higher
- An IDE (e.g., IntelliJ IDEA, Eclipse,vscode)
- Spring Boot Version: 3.2.3

## Setup

1. Clone the repository:
   ```
   git clone https://github.com/mukulgupta0014/BackendProjectCountryApi.git
   ```
2. Navigate to the project directory:
   ```
   cd countrydata
   ```
3. Build the project:
   ```
   mvn clean install
   ```
4. Run the application:
   ```
   mvn spring-boot:run
   ```
## API Endpoints

### Authentication

- **Login**: Generate a JWT token for authentication.
 - **URL**: `http://localhost:8080/auth/login`
 - **Method**: `POST`
 - **Body**:
    ```json
    {
        "username": "user",
        "password": "password"
    }
    ```
 - **Curl Command**:
   ```bash
   curl -X POST http://localhost:8080/auth/login \
   -H "Content-Type: application/json" \
   -d '{"username":"user","password":"password"}'
   ```

### Country Information

- **Get Country Information**: Retrieve information about a specific country.
 - **URL**: `http://localhost:8080/api/country/{countryName}`
 - **Method**: `GET`
 - **Example**: `http://localhost:8080/api/country/india`
 - **Curl Command**:
   ```bash
   curl -X GET http://localhost:8080/api/country/india
   ```

### Language Information

- **Get Language Usage in Countries**: Retrieve information about which countries use a given language.
 - **URL**: `http://localhost:8080/api/lang/{languageName}`
 - **Method**: `GET`
 - **Example**: `http://localhost:8080/api/lang/Hindi`
 - **Curl Command**:
   ```bash
   curl -X GET http://localhost:8080/api/lang/Hindi
   ```

### Sorted Country List

- **Get Sorted Country List**: Retrieve a list of countries sorted by population and area, with optional pagination and sorting order.
 - **URL**: `http://localhost:8080/api/countries/sorted?minPopulation={minPopulation}&minArea={minArea}&pageNumber={pageNumber}&itemsPerPage={itemsPerPage}&sortOrder={sortOrder}`
 - **Method**: `GET`
 - **Example**: `http://localhost:8080/api/countries/sorted?minPopulation=1380004385&minArea=3287590&pageNumber=1&itemsPerPage=1&sortOrder=desc`
 - **Curl Command**:
   ```bash
   curl -X GET "http://localhost:8080/api/countries/sorted?minPopulation=1380004385&minArea=3287590&pageNumber=1&itemsPerPage=1&sortOrder=desc"
   ```

### Country List by Population, Area, and Language

- **Get Country List by Criteria**: Retrieve a list of countries filtered by population, area, and language.
 - **URL**: `http://localhost:8080/api/countries/sorte?minPopulation={minPopulation}&minArea={minArea}&pageNumber={pageNumber}&itemsPerPage={itemsPerPage}&sortOrder={sortOrder}&language={language}`
 - **Method**: `GET`
 - **Curl Command**:
   ```bash
   curl -X GET "http://localhost:8080/api/countries/sorte?minPopulation=329484123&minArea=9372&pageNumber=&itemsPerPage=&sortOrder=desc&language=English"
   ```

## Usage

1. **Authentication**: First, obtain a JWT token by logging in with valid credentials. Use this token in the `Authorization` header for subsequent requests.

2. **API Requests**: Use the provided endpoints to retrieve country information, language usage, and sorted country lists. Ensure to replace placeholders with actual values.

